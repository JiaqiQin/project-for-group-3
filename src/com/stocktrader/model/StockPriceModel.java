package com.stocktrader.model;

public class StockPriceModel {
	
	public double currentPrice = 0.0;
	public double todayOpenPrice = 0.0;
	public double yesterdayClosePrice = 0.0;
	public double maxPrice = 0.0;
	public double minPrice = 0.0;
	
}
