# StockTraderSystem


About this project

This project is aimed to provide a platform to search information of Chinese stock, also to trade stock.

All the data related to the Chinese stock are obtained from Sina Stock API(http://hq.sinajs.cn). If you are required to use any other alternatives of such kind of service, you can modify as you want.

The prerequisites of this project are as follows.

- Operating System: 
Windows 8.1

- Java Environment:
java version "1.8.0_60"
Java(TM) SE Runtime Environment (build 1.8.0_60-b27)
Java HotSpot(TM) 64-Bit Server VM (build 25.60-b23, mixed mode)

- Application Server: 
Tomcat 7.0.64

- Database: 
MySQL Server 5.7

(1) To manage MySQL database manually, a client is needed here.

(2) To build the database at the very beginning, run database.sql, the script which would automatically do everything for you.

(3) To use this project for your own, you need to change the configuration of database.

- Browser: 
Firefox 47.0

All the testing are based on Firefox 47.0. And latest Chrome must be compatible.